<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function index()
    {
        return view('auth.passwords.change');
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'oldPassword' => 'required',
            'password' => 'required|min:6',
            'confirmPassword' => 'required|same:password'
        ]);

        $hashedPassword = Auth::user()->password;
        if(Hash::check($request->oldPassword, $hashedPassword)){
            $user = User::find(Auth::id());
            $user->password = Hash::make($request->password);
            $user->save();
            Auth::logout();
            return redirect(route('login'))->with('success', 'Password has been changed successfully');
        }
        else{
            return redirect()->back()->with('error', 'Invalid current password');
        }
    }
}
