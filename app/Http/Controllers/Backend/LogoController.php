<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Logo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class LogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.logo.index')->with('logos', Logo::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.logo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('image')){
            // creating a instance of the logo class
            $logo = new Logo();
            // assigning the creator id
            $logo->created_by = Auth::user()->id;

            // upload it
            $image = $request->image->store('upload/logos');
            // assign it
            $logo->image = $image;
            $logo->save();
        };

        return redirect(route('logos.index'))->with('success', 'Logo uploaded successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Logo $logo)
    {
        return view('backend.logo.create')->with('logo', $logo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Logo $logo)
    {
        // check if new image
        if($request->hasFile('image')){
            // assigning the updator id
            $logo->updated_by = Auth::user()->id;

            // upload it
            $image = $request->image->store('upload/logos');

            // delete old one
            Storage::delete($logo->image);

            $logo->update(['image' => $image]);

            return redirect(route('logos.index'))->with('success', 'Logo has been updated');
        };
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Logo $logo)
    {
        Storage::delete($logo->image);

        $logo->delete();

        return redirect()->back()->with('error', 'Logo has been Deleted');
    }
}
