<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{

    public function show()
    {
        $id= Auth::user()->id;
        $user = User::findOrFail($id);
        return view('backend.user.profile')->with('user', $user);
    }

    public function edit(User $user)
    {
        return view('backend.user.profile-edit')->with('editData', $user);
    }

    public function update(Request $request, User $user)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$user->id,
        ];

        $this->validate($request, $rules);

        // check if new image
        if($request->hasFile('image')){
            // upload it
            $image = $request->image->store('upload/user_images');

            // delete old one
            Storage::delete($user->image);

            $user->update(['image' => $image]);
        };

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'mobile' => $request->mobile,
            'address' => $request->address,
            'gender' => $request->gender,
        ]);

        return redirect(route('profile'))->with('success', 'Your profile has been updated');
    }
}
