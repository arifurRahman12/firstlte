<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'Frontend\FrontendController@index');
Route::get('/about-us', 'Frontend\FrontendController@aboutUs')->name('aboutUs');
Route::get('/contact-us', 'Frontend\FrontendController@contactUs')->name('contactUs');

Auth::routes();

Route::resource('/users', 'Backend\UserController')->middleware('auth');

Route::resource('/logos', 'Backend\LogoController')->middleware('auth');

Route::group(['prefix' => 'profile', 'middleware' => ['auth']], function(){
    Route::get('/', 'Backend\ProfileController@show')->name('profile');
    Route::get('/{user}/edit', 'Backend\ProfileController@edit')->name('profile.edit');
    Route::put('/{user}', 'Backend\ProfileController@update')->name('profile.update');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function(){
    Route::get('/change-password', 'Auth\ChangePasswordController@index')->name('change.password');
    Route::post('/change-password', 'Auth\ChangePasswordController@update')->name('update.password');
});

