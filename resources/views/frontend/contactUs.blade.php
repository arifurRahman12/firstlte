@extends('frontend.layouts.app')

@section('content')
    <section class="contactUs">
        <img src="{{ asset('frontend/image/banner.jpg') }}" id="banner" class="img-fluid" alt="Responsive image">

        <div class="container my-5">
            <div class="row">
                <div class="col-md-7">
                    <p><u class="h3 text-primary">Send us a message</u></p>
                    <form class="form-row bg-secondary p-3 rounded text-white" action="">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" placeholder="your name">
                            </div>
                            <div class="form-group">
                                <label for="mobile">Mobile No</label>
                                <input type="number" class="form-control" id="mobile" placeholder="your Number" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" placeholder="your email" required>
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="address" class="form-control" id="address" placeholder="your address" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea class="form-control" name="message" rows="3" placeholder="your message"></textarea>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Send Message</button>
                    </form>
                </div>
                <div class="col-md-5">
                    <p><u class="h3 text-primary">Office Location</u></p>
                    <div class="card">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d1824.2972704848257!2d90.40458311610668!3d23.868527425946073!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m1!1m0!5e0!3m2!1sen!2sbd!4v1588660104326!5m2!1sen!2sbd" width="100%" height="370" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
