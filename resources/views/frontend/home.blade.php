@extends('frontend.layouts.app')

@section('content')

    @include('frontend.partials.slider')

    <section class="mission_vision">
        <div class="container">
            <p class="h3 my-3 text-center text-success"><u>Mission & Vision</u></p>
            <div class="row">
                <div class="col-md-6">
                    <img class="img-fluid rounded float-left p-1 mr-3 bg-warning mis_vis" src="{{ asset('frontend/image/mission.jpg') }}" alt="Responsive image">
                    <p id="mis"><strong>Mission</strong> are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free</p>
                </div>
                <div class="col-md-6">
                    <img class="img-fluid rounded float-left p-1 mr-3 bg-warning mis_vis" src="{{ asset('frontend/image/vision.jpg') }}" alt="Responsive image">
                    <p id="vis"><strong>Vision</strong> are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free</p>
                </div>
            </div>
        </div>
    </section>

    <section class="news_events">
        <div class="container">
            <p class="h3 text-center text-primary new_eve py-3"><u>News and Events</u></p>
            <table class="table table-striped table-success">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">SL</th>
                    <th scope="col">Date</th>
                    <th scope="col">Image</th>
                    <th scope="col">Title</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>04/05/2020</td>
                    <td><img src="{{ asset('frontend/image/table.jpg') }}"></td>
                    <td>Dummy Content</td>
                    <td><a class="btn btn-info" href="">Details</a></td>
                </tr><tr>
                    <th scope="row">1</th>
                    <td>04/05/2020</td>
                    <td><img src="{{ asset('frontend/image/table.jpg') }}"></td>
                    <td>Dummy Content</td>
                    <td><a class="btn btn-info" href="">Details</a></td>
                </tr><tr>
                    <th scope="row">1</th>
                    <td>04/05/2020</td>
                    <td><img src="{{ asset('frontend/image/table.jpg') }}"></td>
                    <td>Dummy Content</td>
                    <td><a class="btn btn-info" href="">Details</a></td>
                </tr>
                </tbody>
            </table>
        </div>
    </section>

    <section class="services">
        <div class="container my-5">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Ous Services</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Our Products</a>
                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Experts</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab"><h3>Our Services</h3> passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free</div>
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab"><h3>Our Products</h3> passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free</div>
                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab"><h3>Experts</h3> passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free</div>
            </div>
        </div>
    </section>

@endsection
