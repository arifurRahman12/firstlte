<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/all.css') }}">
</head>
<body>

@include('frontend.partials.header')
@yield('content')
@include('frontend.partials.footer')

<div class="scroll_top">
    <i class="fas fa-angle-double-up fa-3x text-primary"></i>
</div>

<!--    <script src="js/jquery.min.js"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="{{asset('frontend/js/popper.min.js')}}"></script>
<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(window).scroll(function () {
            if($(this).scrollTop()>700){
                $(".scroll_top").fadeIn();
            }else{
                $(".scroll_top").fadeOut();
            }
        });
        $(".scroll_top").click(function () {
            $("html,body").animate({scrollTop:0},1000);
        });
    });
</script>
</body>
</html>

