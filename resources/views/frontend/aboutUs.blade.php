@extends('frontend.layouts.app')

@section('content')
    <section class="aboutUs">
        <img src="{{ asset('frontend/image/banner.jpg') }}" id="banner" class="img-fluid" alt="Responsive image">

        <div class="container text-center">
            <h3 class="text-success my-3"><u>About Us</u></h3>
            <p class="lead">Mission are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free</p>
        </div>
    </section>
@endsection
