<section class="footer bg-dark text-white">
    <div class="container">
        <div class="row py-4">
            <div class="col-md-6">
                <h3>Contact Us</h3>
                <p><i class="fas fa-map-marker-alt fa-2x mt-4 mr-4"></i>Azampur-railgate, Uttara, Dhaka-1230</p>
                <p><i class="fas fa-mobile fa-2x mr-4"></i>+8801675756193</p>
                <p><i class="fas fa-envelope fa-2x mr-4"></i>arifurrrahman1427@gmail.com</p>

            </div>
            <div class="col-md-6">
                <h3>Follow Us</h3>
                <div class="d-inline-block mt-5">
                    <a href="https://www.facebook.com" target="_blank"><i class="fab fa-facebook-square fa-3x mr-3"></i></a>
                    <a href="https://www.youtube.com" target="_blank"><i class="fab fa-youtube fa-3x mr-3"></i></a>
                    <a href="https://www.twitter.com" target="_blank"><i class="fab fa-twitter-square fa-3x mr-3"></i></a>
                    <a href="https://www.google.com" target="_blank"><i class="fab fa-google-plus-square fa-3x"></i></a>
                </div>
            </div>
        </div>
        <p class="text-center my-0 pb-3">Copyright &copy; <script>document.write(new Date().getFullYear())</script> National Geo</p>
    </div>
</section>
