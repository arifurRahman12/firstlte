
@extends('backend.layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Manage Profile</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item">Change Password</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Main row -->
                <div class="row">
                    <!-- Left col -->
                    <section class="col-md-12">
                        <div class="card">
                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif
                            <div class="card-header">
                                <h3>Change Password
                                    <a class="btn btn-success btn-sm float-right" href="{{ route('profile') }}"><i class="fas fa-user"></i> Your Profile</a>
                                </h3>
                            </div>
                            <div class="card-body">
                                <form action="" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="oldPassword">Old Password</label>
                                            <input name="oldPassword" type="password" class="form-control" id="oldPassword" >
                                            @error('oldPassword')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="password">Password</label>
                                            <input name="password" type="password" class="form-control" id="password" >
                                            @error('password')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="confirmPassword">Confirm Password</label>
                                            <input name="confirmPassword" type="password" class="form-control" id="confirmPassword" >
                                            @error('confirmPassword')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                            <input type="submit" value="Change Password" class="btn btn-primary ml-1">
                                        </div>
                                </form>
                            </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection
