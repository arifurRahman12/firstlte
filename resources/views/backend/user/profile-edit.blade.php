
@extends('backend.layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Manage Profile</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('users.index') }}">Home</a></li>
                            <li class="breadcrumb-item">Profile</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Main row -->
                <div class="row">
                    <!-- Left col -->
                    <section class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3>Edit Profile
                                    <a class="btn btn-success btn-sm float-right" href="{{ route('profile') }}"><i class="fas fa-user"></i> Your Profile</a>
                                </h3>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('profile.update', $editData->id ) }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="name">Name</label>
                                            <input name="name" type="text" class="form-control" id="name" value="{{ $editData->name }}">
                                            @error('name')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="email">Email</label>
                                            <input name="email" type="email" class="form-control" id="email" value="{{ $editData->email }}">
                                            @error('email')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="mobile">Mobile</label>
                                            <input name="mobile" type="text" class="form-control" value="{{ $editData->mobile }}">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="address">Address</label>
                                            <input name="address" type="text" class="form-control" value="{{ $editData->address }}">
                                        </div>
                                    <div class="form-group col-md-4">
                                        <label for="gender">Gender</label>
                                        <select name="gender" id="gender" class="custom-select">
                                            <option value="" selected>Select Gender</option>
                                            <option value="male" @if($editData->gender === "male") selected @endif>Male</option>
                                            <option value="female" @if($editData->gender === "female") selected @endif>Female</option>
                                        </select>
                                    </div>
                                        <div class="form-group col-md-4">
                                            <label for="image">Image</label>
                                            <input type="file" name="image" class="form-control-file" id="image">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <img id="showImage" src="{{ (!empty($editData->image)) ? asset('storage/'.$editData->image) : asset('storage/upload/default.png') }}" class="img-fluid" alt="Responsive image" style="max-height: 120px">
                                        </div>
                                        <div class="form-group col-md-6 pt-5">
                                            <input type="submit" value="update" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>
                                    </div>
                                </form>
                            </div>
                    </section>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#image').change(function (e) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#showImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);
            });
        });
    </script>
@endsection
