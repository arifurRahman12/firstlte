
@extends('backend.layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Manage User</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item">{{ isset($user) ? 'Edit User' : 'Add User'}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Main row -->
                <div class="row">
                    <!-- Left col -->
                    <section class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3>{{ isset($user) ? 'Edit User' : 'Add User' }}
                                    <a class="btn btn-success btn-sm float-right" href="{{ route('users.index') }}"><i class="fas fa-list"></i> User List</a>
                                </h3>
                            </div>
                            <div class="card-body">
                                <form action="{{ isset($user) ? route('users.update', $user->id) : route('users.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @if(isset($user))
                                        @method('PUT')
                                    @endif
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="role">User Role</label>
                                            <select name="role" id="role" class="custom-select">
                                                    <option value="" selected>Select Role</option>
                                                @if(isset($user))
                                                    <option value="admin" @if($user->role === "admin") selected @endif>Admin</option>
                                                    <option value="user" @if($user->role === "user") selected @endif>User</option>
                                                @else
                                                    <option value="admin">Admin</option>
                                                    <option value="user">User</option>
                                                @endif

                                            </select>
                                            @error('role')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="name">Name</label>
                                            <input name="name" type="text" class="form-control" id="name" @if(isset($user)) @if($user) value="{{ $user->name }}" @endif  @endif>
                                            @error('name')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="email">Email</label>
                                            <input name="email" type="email" class="form-control" id="email" @if(isset($user)) @if($user) value="{{ $user->email }}" @endif  @endif>
                                            @error('email')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        @if(isset($user))

                                            @else
                                            <div class="form-group col-md-4">
                                                <label for="password">Password</label>
                                                <input name="password" type="password" class="form-control" id="password">
                                                @error('password')
                                                <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="confirmPassword">Confirm Password</label>
                                                <input name="confirmPassword" type="password" class="form-control" id="confirmPassword">
                                                @error('confirmPassword')
                                                <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        @endif

                                        <div class="form-group col-md-6">
                                            <input type="submit" value=" {{ isset($user) ? 'Update' : 'Submit' }}" class="btn btn-primary">
                                        </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection
