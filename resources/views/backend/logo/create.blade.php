
@extends('backend.layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Manage Logo</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item">Logo</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Main row -->
                <div class="row">
                    <!-- Left col -->
                    <section class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3>{{ isset($logo) ? 'Edit Logo' : 'Add Logo'}}
                                    <a class="btn btn-success btn-sm float-right" href="{{ route('logos.index') }}"><i class="fas fa-list"></i> Logo List</a>
                                </h3>
                            </div>
                            <div class="card-body">
                                <form action="{{ isset($logo) ? route('logos.update', $logo->id) : route('logos.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @if(isset($logo))
                                        @method('PUT')
                                    @endif
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="image">Image</label>
                                            <input type="file" name="image" class="form-control-file" id="image">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <img id="showImage" src="{{ (!empty($logo->image)) ? asset('storage/'.$logo->image) : asset('storage/upload/no-logo.png') }}" class="img-fluid" alt="Responsive image" style="max-height: 120px">
                                        </div>
                                        <div class="form-group col-md-6 pt-5">
                                            <input type="submit" value="{{ isset($logo) ? 'Update' : 'Submit' }}" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#image').change(function (e) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#showImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);
            });
        });
    </script>
@endsection
